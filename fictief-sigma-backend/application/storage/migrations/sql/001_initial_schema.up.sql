BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS sigma;

CREATE TABLE sigma.observations (
    id uuid NOT NULL,
    vreemdeling_id TEXT NOT NULL,
    attribute TEXT NOT NULL,
    value JSON NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP,

    PRIMARY KEY(id)
);

COMMIT;
