#!/bin/sh

# Poor-man's integration test.
# Should run without errors after following the `CONTRIBUTING.md` instructions.

set -e

CURL_FLAGS="--fail --globoff --show-error --silent"

echo 'Add vreemdeling to BVV:'
resp=$(echo '{
    "data": {
        "naam": "John Doe",
        "geboortedatum": "1978-12-31"
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen' \
)
printf '%s' "$resp" | jq .
vreemdeling_id=$(printf '%s' "$resp" | jq --raw-output '.data.id')
if [ -z "$vreemdeling_id" -o "$vreemdeling_id" = 'null' ]; then
    echo "No valid vreemdeling id: $vreemdeling_id"
    exit 1
fi

echo 'Get specific vreemdeling from BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id" \
)
printf '%s' "$resp" | jq .

echo 'Search vreemdelingen in BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen?naam=john&geboortedatum=1978-12-31' \
)
printf '%s' "$resp" | jq .

echo 'Add attribute observation to SIGMA:'
resp=$(echo '{
    "data": {
        "attribute": "alias",
        "value": "foo"
    }
}' | curl $CURL_FLAGS \
    -H 'accept: application/json' \
    -X 'POST' \
    --data @- \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id/observations" \
)
printf '%s' "$resp" | jq .

echo 'Get some attributes from a specific SIGMA:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-sigma-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id?attributes=alias,spreektalen" \
)
printf '%s' "$resp" | jq .

echo 'Get some attributes from some SIGMAs:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://shared-loket-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id?attributes=alias,spreektalen&sources=fictief-np" \
)
printf '%s' "$resp" | jq .

echo ''
echo '==========================================='
echo '= Integration tests finished successfully ='
echo '==========================================='
