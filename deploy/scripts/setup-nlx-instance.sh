#!/usr/bin/env bash

set -euo pipefail

# clenaup
rm -rf ca/ certs/
rm -f ca.crt ca.key

# create CA
openssl genrsa -out ca.key 2048
openssl req -x509 -new -nodes -key ca.key -subj "/CN=NLX" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt -config /usr/local/etc/openssl@1.1/openssl.cnf

## init organization certs
wget https://gitlab.com/commonground/nlx/nlx-try-me/-/raw/main/scripts/init-organization-certs.sh
docker run --rm -it -v $(pwd):/workdir -w /workdir --entrypoint /bin/bash cfssl/cfssl:v1.6.4 ./init-organization-certs.sh
rm init-organization-certs.sh
chmod -R 755 ./ca ./certs

# INTERNAL CA
kubectl create secret tls internal-ca \
   --cert=ca.crt \
   --key=ca.key \
   --dry-run=client \
   -o yaml > secret-internal-ca.yaml

# MANAGER
kubectl create secret tls manager-group-tls \
   --cert=certs/org.crt \
   --key=certs/org.key \
   --dry-run=client \
   -o yaml > secret-manager-group-tls.yaml

# INWAY
kubectl create secret tls inway-group-tls \
   --cert=certs/org.crt \
   --key=certs/org.key \
   --dry-run=client \
   -o yaml > secret-inway-group-tls.yaml
